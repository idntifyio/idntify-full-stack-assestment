![Idntify Logo](https://cdn.idntify.io/idntify-logo.png)

# Full Stack Developer Test

Idntify wants to attract people to their site so they gain more popularity as they are the least popular on the KYC field.
The CEO came up with a great idea, launching a site with a free game.

Your task is to build the site for users to play as well as the API that will allow the server to communicate with the client (the site).

---
## Client

The client should:

1. Register a user if it is their first time visiting the page (don't ask the user for any information)
2. Once the user is registered, show the user the instructions to play the game and a start button.
3. Once the user has completed the game, save their score on a database.
4. Once the user has completed all rounds of the game, return the user's score
5. Prompt the user to enter their name, once they have entered their name, save their name on the database as well
6. Show the user a list of users and their scores
7. If the user is on the top of the list, add a badge next to their score or name or anywhere you want.
8. Allow the user to add a friend. Once the list of users is on screen, add an "add friend" button.
9. On the side bar, the user can see a list of friends. From there, he can delete a friend.
10. Allow the user to exit a game.
11. Keep a history of the rounds and scores of the game. Say, user1 played round 1 with a score of 20, user1 played round 1 with a score of 30

---
## API

The API should (have at least the following methods):

1. Expose a method to register the user
2. Expose a method to update the user's score as well as their name
3. Expose a method to retrieve the list of available games
4. Expose a method to retrieve all the users and their scores on the "database"
5. Expose a method to add a friend
6. Expose a method to delete a friend

- Add any other methods required for the Client/Server to maintain data consistency.
- The database **MUST** be **POSTGRES**.

---

#### The whole solution should:
- Be portable, can be built and executed in any system
- Have a clear structure (for both, the API and the Client)
- Be written in React for the frontend, and NodeJS + Express / Hapi for the backend
- Be easy to grow with new functionality 
- Dont include binaries, .sh files, etc. Use a dependency management tool (npm or yarn).

---
#### Bonus Points For:
- Tests, the more coverage the better
- Comments, a good readme, instructions, etc.
- Docker images / CI
- Commit messages (include .git in zip)
- Clear scalability
- ANY other thing you include and adds value to your project

#### Notes
- You should implement only one game.
- For the games logic, no libraries are allowed. Pure JS is required.
- For building the UI and the API you can use any libraries you want

---
#### Games

##### Guess the word
In this game you have to generate a random string of 7 letters. Then, show a "keyboard", this keyboard will include all letters from A-Z. Each letter of the generated string are "hidden" behind little "cards", if the player clicks on a letter of the keyboard and the key is included on the string, reveal the letter on the cards. If it is not, show ANY form of message letting the player know that the key is not in the string. When the game starts, the player has 50 points, everytime the player guesses correctly, +5 points are added, when the player guesses incorrectly, +1 points are substracted. The game must repeat **10 times**


##### Cows and Bulls
In this game, a series of _m_ (with _5_ < _m_ < _10_) randomly generated number of cows and bulls *_n_* are shown on screen (you can represent them as you want) with _5_ < _n_ < _10_ for each cow and bull respectively. Each cow needs 4 square meters and each bull needs 6 square meters to live happily in a farm. The user has to enter the number of square meters needed for the amount of cows and bulls shown on screen. For each cow and bull generated the user has 2 seconds to enter the solution, meaning that if 10 cows and bulls are shown, the user has 20 seconds to enter the space needed for them. If the user enters a wrong solution, +2 seconds are substracted from the total time. If the user guesses correctly, +5 points are added to the score. No initial score is needed on this game (0). The user must be able to see how much time he has left to enter the solution

##### "Blackjack" with dice
In this game, the "AI" rolls three dices (dices are regular dices, each dice has numbers from 1-6), the player can see when each of the dices are rolled and the sum of all of them. Then, the player is allowed to roll ONE dice and then decide if he wants to continue rolling or let the AI play. If the player gets a sum of 9, two dices appear and now he has to roll those two dices, the player can keep rolling as long as he wants or keep his sum and let the "AI" play. If the user gets a sum of more than 21 points after rolling his dices, he losses. If he keeps his sum and decides to let the AI roll, the AI only rolls one dice and if the AI gets a sum of points bigger than the player's sum, the player losses. If the AI gets a sum of more than 21, he losses. The AI always wants to keep rolling until he gets a sum bigger than the user's sum. If the player wins, he gets +10 points to his final score. No initial score is required. This game is repeated **5 times** (to get a max of 50 points)





